//
//  ViewController.m
//  Drudge-SingleView
//
//  Created by Jon Wetherall on 05/06/2015.
//  Copyright (c) 2015 Jon Wetherall. All rights reserved.
//

#import "ViewController.h"
#import "CDVViewController.h"
#import "AppDelegate.h"
#import "CustomWebView.h"

@import Foundation;
@import UIKit;


#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

@interface ViewController ()
@property (nonatomic) int adViewWidth;
@property (nonatomic) int adViewHeight;
@property (strong, nonatomic) CDVViewController *cdvViewController;
@property (strong, nonatomic) CustomWebView *webView;
@property (strong, nonatomic) OXMAdView* bannerAdView;
@property (strong, nonatomic) OXMAdView* interstitialAdView;
@property BOOL interstitialAdViewReady;
@property int linkClicksTillInterstitialDisplay;

@end

@implementation ViewController
@synthesize homeButton;
 Reachability *internetReachableFoo;

CDVViewController* viewController ;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Top Banner, visible during link click
    self.bannerAdView = [OXMAdView new];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //iPad
        self.bannerAdView.domain = @"imk-d.openx.net";
        self.bannerAdView.adUnitId = @"538181247";
        self.adViewWidth = 728;
        self.adViewHeight = 90;
    } else {
        //iPhone
        self.bannerAdView.domain = @"imk-d.openx.net";
        self.bannerAdView.adUnitId = @"538741627";
        self.adViewWidth = 320;
        self.adViewHeight = 50;
    }
    self.bannerAdView.frame = CGRectMake(self.view.bounds.size.width/2 - self.adViewWidth/2, 20, self.adViewWidth, self.adViewHeight);
    self.bannerAdView.hidden = YES;
    self.bannerAdView.backgroundColor = [UIColor blackColor];
    
    //Interstitial Ad, visible after first link click and every 3rd thereafter
    self.interstitialAdView = [OXMAdView new];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //iPad
        self.interstitialAdView.domain = @"imk-d.openx.net";
        self.interstitialAdView.adUnitId = @"538780622";
    } else {
        //iPhone
        self.interstitialAdView.domain = @"imk-d.openx.net";
        self.interstitialAdView.adUnitId = @"538780623";
    }
    self.interstitialAdView.delegate = self;
    self.interstitialAdView.autoDisplayOnLoad = false;
    OXMInterstitialDisplayProperties* interstitialDisplayProperties = [OXMInterstitialDisplayProperties new];
    interstitialDisplayProperties.closePosition = OXMInterstitalClosePostitionTopRight;
    interstitialDisplayProperties.closeButtonImage = [UIImage imageNamed:@"closeButton"];
    self.interstitialAdView.interstitialDisplayProperties = interstitialDisplayProperties;
    self.interstitialAdViewReady = NO;
    self.linkClicksTillInterstitialDisplay = 2;
    [self.interstitialAdView load];
    
    
    [self testInternetConnection];
    self.cdvViewController = [CDVViewController new];
    self.cdvViewController.view.frame = CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height - 20);
    [self.view addSubview:self.cdvViewController.view];
    //self.cdvViewController.webView.autoresizesSubviews = YES;
    self.cdvViewController.webView.delegate = self;
   // [UIViewController attemptRotationToDeviceOrientation];
    
    
    self.webView = [CustomWebView new];
    self.webView.frame = CGRectMake(0, 20 + self.adViewHeight,self.view.bounds.size.width, self.view.bounds.size.height - 20 - self.adViewHeight);
   // self.webView.autoresizesSubviews = YES;
    [self.view addSubview:self.bannerAdView];
    [self.view addSubview:self.webView];
    self.webView.delegate =self.webView;
    self.webView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    
    self.cdvViewController.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    

    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    homeButton.enabled = false;
    
    [self setContent];
    

 
    
}


- (void) setContent
{
    
   /* NSString *urlString = @"https://ssltest.imktechnology.com/wp-content/uploads/2016/06/TopHeadlines.html";
    NSURL *myURL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *string = [NSString stringWithContentsOfURL:myURL encoding:NSUTF8StringEncoding error:nil];
    string = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    NSString * jsCallBack = [NSString stringWithFormat:@"setTopBar('%@');",string];
    [ _cdvViewController.webView stringByEvaluatingJavaScriptFromString:jsCallBack];
    
    urlString = @"https://ssltest.imktechnology.com/wp-content/uploads/2016/06/HeadlinesColumn1.html";
    myURL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    string = [NSString stringWithContentsOfURL:myURL encoding:NSUTF8StringEncoding error:nil];
    
    //string = [[string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet u]];
  NSString * jsCallBack2 = [NSString stringWithFormat:@"setLeftCol('%@');",string];
    [ _cdvViewController.webView stringByEvaluatingJavaScriptFromString:jsCallBack2];*/
    
}

- (void) didRotate:(NSNotification *)notification
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    self.bannerAdView.frame = CGRectMake(self.view.bounds.size.width/2 - self.adViewWidth/2, 20, self.adViewWidth, self.adViewHeight);
    self.webView.frame = CGRectMake(0, 20 + self.adViewHeight, self.view.bounds.size.width, self.view.bounds.size.height - 20 - self.adViewHeight);

   
    
    
    if (orientation == UIDeviceOrientationLandscapeLeft)
    {
        NSLog(@"Landscape Left!");
    }
}

-(BOOL)shouldAutorotate {
    return YES;
}



// Checks if we have an internet connection or not
- (void)testInternetConnection
{
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Yayyy, we have the interwebs!");
        });
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No WIFI available!", @"alertView")
                                                                    message:NSLocalizedString(@"You have no wifi connection available. Please connect to a WIFI network.", @"alertView")
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"OK", @"AlertView")
                                                          otherButtonTitles:NSLocalizedString(@"Open settings", @"alertView"), nil];
                [alertView show];
            }
        });
    };
    
    [internetReachableFoo startNotifier];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];

    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=AIRPLANE_MODE"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // code here will be executed before rotation
    NSString* path = [[NSBundle mainBundle] pathForResource:@"www/index" ofType:@"html"];
    NSURL* url = [NSURL fileURLWithPath:path];
    NSURLRequest* request = [NSURLRequest requestWithURL: url];
    
    [_cdvViewController.webView loadRequest: request ];
}*/

-(void) resetWebView {
    
    
    self.webView.hidden = YES;
    self.bannerAdView.hidden = YES;
    [self.webView removeFromSuperview];
    self.webView = nil;
    
    self.webView = [CustomWebView new];
    self.webView.frame = CGRectMake(0, 20 + self.adViewHeight, self.view.bounds.size.width, self.view.bounds.size.height - 20 - self.adViewHeight);
    self.webView.autoresizesSubviews = YES;
    [self.view addSubview:self.webView];
    self.webView.delegate =self.webView;
    self.webView.hidden = YES;
    

}

- (IBAction)back:(id)sender
{
    // self.webView.hidden = YES;
    
  /*  if ( IDIOM == IPAD ) {
        //[ _cdvViewController.webView goBack];
        
        if (self.webView.canGoBack == YES) {
            [self.webView goBack];
        } else {
            
            self.webView.hidden = YES;
            NSString *urlText = @"http://www.drudgereport.com";
          
            
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:urlText]];
            [_cdvViewController.webView loadRequest:requestObj];
            
        }
        
        
        
    } else {*/
     
    if (self.webView.canGoBack == YES) {
        [self.webView goBack];
    }
    else {
         homeButton.enabled = false;
        [self resetWebView];
        
        [self.cdvViewController.webView stringByEvaluatingJavaScriptFromString:@"setupOpenXAds();"];
    }
        
  //  }
    
}

- (IBAction)refresh:(id)sender
{
    NSString *currentURL = _cdvViewController.webView.request.URL.absoluteString;
    
    NSURL *url = [NSURL URLWithString:currentURL];
      NSURLRequest* request = [NSURLRequest requestWithURL: url];
    self.webView.hidden = YES;
    
     [ _cdvViewController.webView stringByEvaluatingJavaScriptFromString:@"clearCacheAndRefresh();"];
    
    NSString *message = @"Refreshing Drudge Site";
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil                                                            message:message
                                                                                                                delegate:nil                                                  cancelButtonTitle:nil                                                  otherButtonTitles:nil, nil];
    toast.backgroundColor=[UIColor redColor];
    [toast show];
    int duration = 2; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{                [toast dismissWithClickedButtonIndex:0 animated:YES];            });
    
   // [ _cdvViewController.webView stringByEvaluatingJavaScriptFromString:@"getContent();"];


   // [ _cdvViewController.webView loadRequest:request];
 
    [self setContent];

    
    
   }

-(IBAction)openInSafari: (id) sender
{
  //  NSString *currentURL = _cdvViewController.webView.request.URL.absoluteString;
    NSString *currentURL = self.webView.request.URL.absoluteString;
    
 
   // NSRange rangeValue = [currentURL rangeOfString:@"drudgereportw" options:NSCaseInsensitiveSearch];
    //NSRange rangeValue = [currentURL rangeOfString:@"drudge.app" options:NSCaseInsensitiveSearch];
    
    
    //if (rangeValue.length > 0){
    if (currentURL == nil){
       currentURL = @"http://www.drudgereport.com";
        
    }
    
    NSURL *url = [NSURL URLWithString:currentURL];
    
    
    [[UIApplication sharedApplication] openURL:url];
 
}
- (IBAction)home:(id)sender
{
   // NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: @"index.html"] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: myTimeoutValue];
    //NSString* path = [[NSBundle mainBundle] pathForResource:@"www/index" ofType:@"html"];
    
    /*if ( IDIOM == IPAD ) {
        self.webView.hidden = YES;
     NSURL* url = [NSURL URLWithString:@"http://www.drudgereport.com"];
    NSURLRequest* request = [NSURLRequest requestWithURL: url];
        [_cdvViewController.webView loadRequest: request ];
    } else {*/
    NSString* path = [[NSBundle mainBundle] pathForResource:@"www/index" ofType:@"html"];
    NSURL* url = [NSURL fileURLWithPath:path];
    NSURLRequest* request = [NSURLRequest requestWithURL: url];
    
    [_cdvViewController.webView loadRequest: request ];
    [self resetWebView];
     homeButton.enabled = false;
  //  }
  //  [_cdvViewController]
}

// UIWebView delegates

- (void)webViewDidStartLoad:(UIWebView*)theWebView {
    [self.cdvViewController webViewDidStartLoad:theWebView];
}

- (void)webViewDidFinishLoad:(UIWebView*)theWebView {
    [self.cdvViewController webViewDidFinishLoad:theWebView];
}

- (void)webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error {
    [self.cdvViewController webView:theWebView didFailLoadWithError:error];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if (![self.cdvViewController webView:webView shouldStartLoadWithRequest:request navigationType:navigationType]) {
        return NO;
    }
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
    
        self.linkClicksTillInterstitialDisplay--;
        
        if (self.interstitialAdViewReady && self.linkClicksTillInterstitialDisplay <= 0) {
            [self.interstitialAdView showAsInterstitialFromRoot:self];
            self.linkClicksTillInterstitialDisplay = 3;
        }
        
         homeButton.enabled = true;
        self.webView.hidden = NO;
        self.bannerAdView.hidden = NO;
        [self.bannerAdView load];
        NSURL *url = [request URL];
            
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:requestObj];
        
        return NO;
        
    }
    return YES;
    
}

#pragma mark - OXMAdViewDelegate

- (void)adDidLoad:(OXMAdView * _Nonnull)adView {
    self.interstitialAdViewReady = YES;
}

- (void)adDidFailToLoad:(OXMAdView * _Nonnull)adView error:(NSError * _Nonnull)error {
    self.interstitialAdViewReady = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.interstitialAdView load];
    });
}

- (void)adInterstitialDidClose:(OXMAdView * _Nonnull)adView {
    [self.interstitialAdView load];
}




@end
