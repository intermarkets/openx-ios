//
//  OpenXSDKCoreCordovaPlugin.h
//  Drudge-SingleView
//
//  Created by Scott McCoy on 3/1/17.
//  Copyright © 2017 Jon Wetherall. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

//Superclass
#import "CDV.h"

@interface OpenXSDKCoreCordovaPlugin : CDVPlugin <CLLocationManagerDelegate>
- (void)getAdRequestData:(CDVInvokedUrlCommand *)command;
@end
