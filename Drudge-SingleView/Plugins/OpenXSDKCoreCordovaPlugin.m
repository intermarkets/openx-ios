//
//  OpenXSDKCoreCordovaPlugin.m
//  Drudge-SingleView
//
//  Created by Scott McCoy on 3/1/17.
//  Copyright © 2017 Jon Wetherall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenXSDKCoreCordovaPlugin.h"
#import <objc/runtime.h>
#import <AdSupport/AdSupport.h>


@interface OpenXSDKCoreCordovaPlugin ()

@property (nonatomic, strong) CLLocation* currentLocation;
@property (nonatomic, strong) CLLocationManager* locationManager;

@end


@implementation OpenXSDKCoreCordovaPlugin

- (void)pluginInitialize {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startUpdatingLocation];
    });
}


- (void)getAdRequestData:(CDVInvokedUrlCommand *)command {
    CDVPluginResult* pluginResult;

    if (!command.arguments || command.arguments.count < 5) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Invalid arguments"];
    }

    NSMutableDictionary* adRequestData = [NSMutableDictionary new];

    // Get IDFA
    adRequestData[@"did.ia"] = [ASIdentifierManager sharedManager].advertisingIdentifier.UUIDString;
    adRequestData[@"did.iat"] = [NSNumber numberWithBool:[ASIdentifierManager sharedManager].isAdvertisingTrackingEnabled];

    // Get GPS
    if (self.currentLocation) {
        adRequestData[@"lat"] = [NSNumber numberWithDouble:self.currentLocation.coordinate.latitude];
        adRequestData[@"lon"] = [NSNumber numberWithDouble:self.currentLocation.coordinate.longitude];
        adRequestData[@"lt"] = @1;
    }

    NSArray* resultArray = @[
        command.arguments[0],
        command.arguments[1],
        command.arguments[2],
        command.arguments[3],
        command.arguments[4],
        adRequestData
    ];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsMultipart:resultArray];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations {
    self.currentLocation = [locations lastObject];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error {
    [manager stopUpdatingLocation];
}

@end


