//
//  PlayerDetailsViewControllerTableViewController.h
//  Drudge-SingleView
//
//  Created by Jon Wetherall on 05/06/2015.
//  Copyright (c) 2015 Jon Wetherall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerDetailsViewControllerTableViewController : UITableViewController

@end
