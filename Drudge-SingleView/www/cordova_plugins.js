cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-google-analytics/www/analytics.js",
        "id": "cordova-plugin-google-analytics.UniversalAnalytics",
        "clobbers": [
            "analytics"
        ]
    },
    {
        "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
        "id": "cordova-plugin-inappbrowser.inappbrowser",
        "clobbers": [
            "cordova.InAppBrowser.open",
            "window.open"
        ]
    },
	{
		"file": "plugins/openx/www/openx.js",
		"id": "openx",
		"clobbers": [
			"openx"
		]
	}    
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.google.playservices": "19.0.0",
    "cordova-plugin-google-analytics": "0.7.2",
    "cordova-plugin-inappbrowser": "1.0.1-dev",
    "cordova-plugin-whitelist": "1.0.0"
}
// BOTTOM OF METADATA
});