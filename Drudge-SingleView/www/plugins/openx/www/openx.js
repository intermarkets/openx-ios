cordova.define("openx", function(require, exports, module) {

    var argscheck = require('cordova/argscheck'),
        exec = require('cordova/exec'),
        openxExport = {},
        adRequestTemplate = 'http://%%domain%%/ma/1.0/arh?auid=%%auid%%&app.bundle=net.intermarkets.drudge';

    function onGetAdRequestDataSuccess(domID, domain, adUnitId, width, height, reqData) {
        var ifr,
            containerEl = document.getElementById(domID),
            reqURL = adRequestTemplate.replace(/%%domain%%/, domain)
                                      .replace(/%%auid%%/, adUnitId);

        for (var prop in reqData) {
            if (reqData.hasOwnProperty(prop)) {
                reqURL += '&' + prop + '=' + encodeURIComponent(reqData[prop]);
            }
        }

        ifr = document.createElement('iframe');
        ifr.src = reqURL;
        ifr.width = width;
        ifr.height = height;
        ifr.marginWidth = 0;
        ifr.marginHeight = 0;
        ifr.frameSpacing = 0;
        ifr.frameBorder = 'no';
        ifr.scrolling = 'no';

        // Remove any existing content. Should be safer than changing innerHTML
        // in the event there are listeners attached to any nodes.
        while (containerEl.hasChildNodes()) {
            containerEl.removeChild(containerEl.lastChild());
        }

        document.getElementById(domID).appendChild(ifr);
    }

    function onGetAdRequestDataFailure(msg) {
        console.log(msg);
    }

    openxExport.getBanner = function(domID, domain, adUnitId, width, height) {
       cordova.exec(
           onGetAdRequestDataSuccess,
           onGetAdRequestDataFailure,
           'OpenXSDKCoreCordovaPlugin',
           'getAdRequestData',
           [domID, domain, adUnitId, width, height]
        );
    }

    module.exports = openxExport;

});
